import os
import requests
from requests.adapters import HTTPAdapter
from requests.exceptions import RetryError
from urllib3.util.retry import Retry

TOKEN = os.environ["TOKEN"]
SENTROPY_DETECT_URL = "https://api.sentropy.io/v1"

session = requests.Session()
session.headers.update(
    {"Content-Type": "application/json", "Authorization": f"Bearer {TOKEN}"}
)
retry = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[429, 502, 503, 504],
    allowed_methods=["POST"],
)
session.mount("https://", HTTPAdapter(max_retries=retry))

# Fill in actual text, unique id, author, and segment
payloads = [
    {
        "text": f"query {i}",
        "id": f"query {i}",
        "author": "sentropy",
        "segment": "sentropy",
    }
    for i in range(100)
]

for payload in payloads:
    try:
        response = session.post(SENTROPY_DETECT_URL, json=payload)
        print(response.text)
    except RetryError as e:
        print("Max Retry")
        raise

